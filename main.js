import tabJoursEnOrdre from "./gestionTemp.js";

const CLEAPI = "2c35f8e6dfa576e3adc79ad36463743a";
let resultatsAPI;
const temps = document.querySelector('.temps');
const temperature = document.querySelector('.temperature');
const localisation = document.querySelector('.localisation');
const heure = document.querySelectorAll('.heure-nom-prevision');
const tempPourH = document.querySelectorAll('.heure-prevision-valeur');
const jourDiv = document.querySelectorAll(".jour-prevision-nom");
const tempJourDiv=document.querySelectorAll(".jour-prevision-temp");
const imageIcone=document.querySelector(".logo-meteo");
const chargementContainer=document.querySelector(".overlay-icone-chargement");



if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
        let long = position.coords.longitude;
        let lat = position.coords.latitude;
        AppelAPI(long, lat);
    }, () => {
        alert("Vous avez refusé la géolocalisation,l'application ne peut pas fonctionner, veuillez l'activez .!");
    })
}


function AppelAPI(long, lat) {
    fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${long}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPI}`)
        .then((reponse) => {
            return reponse.json();
        })
        .then((data) => {
            resultatsAPI = data;

            console.log(resultatsAPI);
            temps.innerText = resultatsAPI.current.weather[0].description;
            temperature.innerText = `${Math.trunc(resultatsAPI.current.temp)}°`;
            localisation.innerText = resultatsAPI.timezone;

            //les heures par trenche de trois , avec leur temperature.
            let heureActuelle = new Date().getHours();

            for (let i = 0; i < heure.length; i++) {

                let heureInc = heureActuelle + i * 3;

                if (heureInc > 24) {
                    heure[i].innerText = `${heureInc - 24} h`;
                } else if (heureInc === 24) {
                    heure[i].innerText = "00 h";
                } else {
                    heure[i].innerText = `${heureInc} h`;
                }
            }

            //temp pour 3h
            for (let j = 0; j < tempPourH.length; j++) {
                tempPourH[j].innerText = `${resultatsAPI.hourly[j * 3].temp}°`;
            }

            //trois premiers lettres des jours
            for (let k = 0; k < tabJoursEnOrdre.length; k++) {
                jourDiv[k].innerText = tabJoursEnOrdre[k].slice(0, 3);
            }
            //Temp par jour

            for(let m=0;m<7;m++){
                tempJourDiv[m].innerText=`${Math.trunc(resultatsAPI.daily[m+1].temp.day)}°`;
            }

            // Icone dynamique

        let nameIcon=resultatsAPI.current.weather[0].icon;
        
            if(nameIcon.search("d")!=-1){
              imageIcone.src=`ressources/jour/${resultatsAPI.current.weather[0].icon}.svg`; 
            }
              else{
                  imageIcone.src=`./ressources/nuit/${resultatsAPI.current.weather[0].icon}.svg`;
              }
            
              chargementContainer.classList.add('disparition');

        })
}
